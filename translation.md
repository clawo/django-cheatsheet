# How to make a Django API multilingual

## Using the Modeltranslation package
 - install the package: `pipenv install django-modeltranslation`
 - add it to installed apps in settings.py
 - add Languages to settings.py:
 ```python
from django.utils.translation import ugettext_lazy as _
...

LANGUAGES = (
    ('en', _('English')),
    ('de', _('German')),
)
 ```

 - create translation.py file in app directory and insert following code in order to translate the field `name` of the model `EnergySource`:
 ```python
from modeltranslation.translator import translator, TranslationOptions
from helper.co2calculator.models import EnergySource


class EnergySourceTranslationOptions(TranslationOptions):
    fields = ('name',)


translator.register(EnergySource, EnergySourceTranslationOptions)
```
- then create and apply migrations. a new migration should be created which adds additional fields to the registered model.
- the EnergySource model now has two additional fields: `name_en` and `name_de`
- example: opening a django shell and setting the name of an energy source:
```python
from helper.co2calculator.models import EnergySource
e = EnergySource.objects.all()[0]
e.name = "english name"
e.name
>>> 'english name'
e.name_en
>>> 'english name'
e.name_de
>>> #not set yet, so nothing will be returned
e.name_de = 'deutscher name'
e.name
>>> 'english name'
e.name_de
>>> 'deutscher name'
# now change the current language as follows:
from django.utils.translation import activate
activate('de')
# test again:
e.name
>>> 'deutscher name' #yay!
```
### Thoughts for next projects:
- use models/foreign key fields instead of choices, when multiple languages might be implemented. then the whole backend should be translatable with modeltranslation. 
- e.g. cooling -- AcSplitUnitMeasure has choices for building_type field ('residential' vs. 'non-residential'). unclear how this could be translated with modeltranslation. if it had a foreign key field instead, linking to a submodel BuildingType with two instances which each have a name ('residential' and 'non-residential'), these names could be translated using modeltranslator.

## Add settings to automatically detect browser language
https://medium.com/fueled-engineering/becoming-a-multilingual-super-hero-in-django-part-1-a000101514dd
- add the folowing to settings.py:
```python
from django.utils.translation import ugettext_lazy as _

...

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware', # locale middleware needs to go after session and before common
    'django.middleware.common.CommonMiddleware',
    ...
]

# set the following to true, if its not already set
USE_I18N = True

USE_L10N = True

USE_TZ = True
```
- now when setting a german and english name for EnergySource instances and accessing the energy source API endpoint, the name should be displayed according to the browsers language.

### Open questions:
- How do we want the language to be set? through a button? languages can be determined by a language url pattern, to do that: use the i18n_patterns function in your root URLconf.

## Translate static data
- e.g. choices for a model field (?)
https://medium.com/fueled-engineering/becoming-a-multilingual-super-hero-in-django-part-2-b509a3f2f4a0

- in any python file (e.g. models or views), do the following to mark text to be translated:
```python
from django.utils.translation import ugettext_lazy
...
BUILDING_TYPES = (
    (0, ugettext_lazy('residential')), #ugettetxt_lazy() marks the content as to be translated. in views, use ugettext(...) instead
    (1, ugettext_lazy('non-residential')),
)
....

@classmethod()
def get_schema(cls):
...
building_type = RadioSelectGenerator(
            name="building_type",
            long_title="What is the type of the building?",
            items=[
                {'const': 'residential', 'title': BUILDING_TYPES[0][-1]}, #referring to the translated version of the choices matrix
                {'const': 'non-residential', 'title': BUILDING_TYPES[1][-1]},
            ],
            default='residential',
        ).schema
...
```
- do this wherever something shall be translated. then generate the .po file (must be done for each language separately):
```zsh
$ python manage.py makemessages -l 'zh_CN'
```
- you might have to install gettext on your operating system (`sudo apt install gettext`). probably necessary to include in docker setup too.
- now there should be a new directory 'locales' with a file `django.po`
- in this file, fill in the translation as follows:
```
...
#: apps/cooling/models.py:11
msgid "residential"
msgstr "Wohnhaus"

#: apps/cooling/models.py:12
msgid "non-residential"
msgstr "Anderes Gebaeude"
...
```
- run `python manage.py compilemessages` to compile the .po file. this creates a .mo file.
- restart server, then it should work.
- to add new translations and update the .po file, use: `python manage.py makemessages -a`, then re-compile. (only works to add translations for already existing languages)

### Thoughts/questions
- using ugettetxt_lazy in json schema makes the schema validator test fail, because the translated text is not a string but different type. check whether this is a problem with frontend. response of api endpoint seems to be ok.
