# Versioning approaches

## Copying an existing django app as v2
- e.g. copying the app cooling as cooling_v2: 
    - register that app in settings.py
    - adapt name in cooling_v2/apps.py
    - delete the migrations folder in cooling_v2 and make new migrations
    - check all imports in views, model, urls, tests etc., e.g. in cooling_v2/urls.py replace `from apps.cooling.views import AcSplitUnitMeasureViewSet` with `from apps.cooling_v2.views import AcSplitUnitMeasureViewSet`
    - update `self.url` and `self.url_name` in the view tests of cooling_v2
    - change url basename in urls.py and adapt view tests
    - adapt fixtures like this:
    ```json
    {
        "model": "cooling_v2.acefficiencyrating",
        ...
    }
    ```
    - if fixtures remain unchanged, it could be an option to load and work with v1 fixtures, but might become messy...
    - add the v2 url to the projects urls.py: 
    ```python
    urlpatterns = [
        ...
        path('api/cooling/', include('apps.cooling.urls')),
        path('api/v2/cooling/', include('apps.cooling_v2.urls')),
        ...
    ]
    ```
