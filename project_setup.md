# How to set up a new project with Django, Vue and Docker

## General
- create project directory plus subdirectories `backend`, `frontend`, `nginx`
- create `docker-compose.yml` file in project directory and a `Dockerfile` in each subdirectory
- create `.env` and `.gitignore` files in project directory

## Backend subdirectory: Django
- create `requirement.txt` file, add Django with a version to it, e.g.:
```txt
Django==3.2.2
```
- start a new django project (either install requirements in a venv and run django-admin startproject from there, or start project in docker container...)
- create subsirectory `scripts` and add `start.sh` and `entrypoint.sh`
