# Django Cheat Sheet

## Create a new django project
- open terminal, create project directory and change into that dir
- start virtual environment with pipenv: ```pipenv shell```
(this should create a pipfile and lead to no results when typing ```pip3 freeze```)
- install django in the virtual environment: ```pipenv install django```
- create new project with ``` django-admin startproject PROJECTNAME . ``` (will create a new directory for that project named after the PROJECTNAME, containing a couple of files and a subdirectory also called PROJECTNAME)
- change to the new directory (i.e. where the manage.py file is)
- create new app: create app directory, run ` django-admin startapp APPNAME path/to/appdirectory`, then add the name to `INSTALLED_APPS` in `settings.py` (in docker it must be done as django admin and from non-docker shell, otherwise it is impossible to save anything...)

## Add a Vue app to the django project
- install vue-cli init in virtual environment: `npm i -g @vue/cli-init`
- run `vue init webpack frontend` and choose following settings:

    - Project name: frontend
    - Project description: A Vue.js project
    - Author: Your Name your.name@somewhere.com
    - Vue build: Runtime + Compiler: recommended for most users
    - Install vue-router? Yes
    - Use ESLint to lint your code? Yes
    - Pick an ESLint preset: Standard
    - Set up unit tests: No
    - Setup e2e tests with Nightwatch? No
    - Should we run npm install for you after the project has been created? (recommended) Yes, use NPM

## Vue Commands
- install dependencies: `npm install`
- run development server: `npm run dev`

## Terminal Commands
- Open a Django Shell: ``` python manage.py shell ```
- Open a Django Shell with all files, models etc. preloaded: `python manage.py shell_plus --ipython`
- Make migrations: ``` python manage.py makemigrations ```
- Migrate ``` python manage.py migrate ```
- start Server `python manage.py runserver`

## Django shell Commands
- All instances of a model MyModel: ``` MyModel.objects.all()```
- Create a new instance: ``` instance = MyModel(field1=value1, field2=value2...)```
- Access values of the instance: ``` instance.field1 ```
- Save instance: ``` instance.save() ```
- Dump all model instances to a json `python manage.py dumpdata [--indent=4] APPNAME.ModelName > path/name.json`
(try to do this from outside docker, otherwise trouble with manually adding fixtures and saving the file)
- Load data from fixture `python manage.py loaddata path/name.json`

## Django Migrations
- undo all migrations after a certain one:`python manage.py migrate my_app 0010_previous_migration`


## Problems with python path
- problem: in a virtual environment, python does not find packages that are already installed ("module not found")
- solution: in vs code, change the used version of python on the bottom to the one that uses the correct virtual environment. then reinstall the pipfile and it should work.

## Pip Commands
- install requirements: `pip install -r requirements.txt`

## Remove all instances and reset primary keys of a model
- run `python manage.py migrate APPNAME zero` (deletes ALL model instances of this app!)
- migrate again and create new model instances. they will start with pk=1
## Python tricks
- `def splitComma(line: str) -> str:` function with annotation. python saves it as a hint and doesnt process what comes after colon or ->, but other programs might.
- Getters, setters and deleters through decorators:
```python
class House(object):
    def __init__(self, price):
        self._price = price

    # getter
    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, new_price):
        self._price = new_price
        #can include validations etc.

    @price.deleter
    def price(self):
        del self._price
```
used like normal public attributes:
```python
house.price
house.price = 12345
del house.price
```


